<!DOCTYPE html>
<html lang="en">
<!-- container-fluid -->
<head>
    <title><?php echo isset($title) ? $title : "Admin Panel -Net City"; ?></title>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="stylesheet" href="assets/css/bootstrap.min.css" />
    <link rel="stylesheet" href="assets/css/bootstrap-responsive.min.css" />
    <link rel="stylesheet" href="assets/css/fullcalendar.css" />
    <link rel="stylesheet" href="assets/css/unicorn.main.css" />
    <link rel="stylesheet" href="assets/css/unicorn.grey.css" class="skin-color" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" /></head>
<body>


<div id="header">
    <h1 style="color: #fff;">Admin</h1>
</div>


<div id="user-nav" class="navbar navbar-inverse">
    <ul class="nav btn-group">
        <li class="btn btn-inverse"><a title="" href="#"><i class="icon icon-user"></i> <span class="text">Profile</span></a></li>

        <li class="btn btn-inverse"><a title="" href="#"><i class="icon icon-lock"></i> <span class="text">Change Password</span></a></li>


        <li class="btn btn-inverse"><a title="" href="login.php"><i class="icon icon-share-alt"></i> <span class="text">Logout</span></a></li>
    </ul>
</div>


<?php include 'sidebar.php';?>




<div id="content">
    <div id="content-header">
            <h1>Dashboard</h1>

    </div>
    <div id="breadcrumb">
        <a href="#" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a>
        <a href="#" class="current">Dashboard</a>
    </div>