<div id="sidebar">
    <a href="#" class="visible-phone"><i class="icon icon-home"></i> Net City Admin</a>
    <ul>
        <li class="active"><a href="index.php"><i class="icon icon-home"></i> <span> Net City Admin</span></a></li>
        <li class="submenu">
            <a href="#"><i class="icon icon-th-list"></i> <span>Form elements</span> <span class="label">3</span></a>
            <ul>
                <li><a href="form-common.php">Common elements</a></li>
                <li><a href="form-validation.php">Validation</a></li>
                <li><a href="form-wizard.php">Wizard</a></li>
            </ul>
        </li>
        <li><a href="buttons.php"><i class="icon icon-tint"></i> <span>Buttons &amp; icons</span></a></li>
        <li><a href="interface.php"><i class="icon icon-pencil"></i> <span>Interface elements</span></a></li>
        <li><a href="tables.php"><i class="icon icon-th"></i> <span>Tables</span></a></li>
        <li><a href="grid.php"><i class="icon icon-th-list"></i> <span>Grid Layout</span></a></li>
        <li class="submenu">
            <a href="#"><i class="icon icon-file"></i> <span>Sample pages</span> <span class="label">4</span></a>
            <ul>
                <li><a href="invoice.php">Invoice</a></li>
                <li><a href="chat.php">Support chat</a></li>
                <li><a href="calendar.php">Calendar</a></li>
                <li><a href="gallery.php">Gallery</a></li>
            </ul>
        </li>
        <li>
            <a href="charts.php"><i class="icon icon-signal"></i> <span>Charts &amp; graphs</span></a>
        </li>
        <li>
            <a href="widgets.php"><i class="icon icon-inbox"></i> <span>Widgets</span></a>
        </li>
    </ul>

</div>