<!--Header-->
<?php include 'component/header.php';?>
			<div class="container-fluid">
				<div class="row-fluid">
					<div class="span12">
						<div class="widget-box">
							<div class="widget-title">
								<span class="icon">
									<i class="icon-align-justify"></i>									
								</span>
								<h5>Text inputs</h5>
							</div>
							<div class="widget-content nopadding">
								<form action="#" method="get" class="form-horizontal" />
									<div class="control-group">
										<label class="control-label">Normal text input</label>
										<div class="controls">
											<input type="text" />
										</div>
									</div>
									<div class="control-group">
										<label class="control-label">Password input</label>
										<div class="controls">
											<input type="password" />
										</div>
									</div>
									<div class="control-group">
										<label class="control-label">Input with description</label>
										<div class="controls">
											<input type="text" />
											<span class="help-block">This is a description</span>
										</div>
									</div>
									<div class="control-group">
										<label class="control-label">Input with placeholder</label>
										<div class="controls">
											<input type="text" placeholder="This is a placeholder..." />
										</div>
									</div>
									<div class="control-group">
										<label class="control-label">Normal textarea</label>
										<div class="controls">
											<textarea></textarea>
										</div>
									</div>
									<div class="form-actions">
										<button type="submit" class="btn btn-primary">Save</button>
									</div>
								</form>
							</div>
						</div>						
					</div>
				</div>
				<div class="row-fluid">
					<div class="span12">
						<div class="widget-box">
							<div class="widget-title">
								<span class="icon">
									<i class="icon-align-justify"></i>									
								</span>
								<h5>Rest of elements...</h5>
							</div>
							<div class="widget-content nopadding">
								<form action="#" method="get" class="form-horizontal" />
									<div class="control-group">
										<label class="control-label">Select input</label>
										<div class="controls">
											<select>
												<option />First option
												<option />Second option
												<option />Third option
												<option />Fourth option
												<option />Fifth option
												<option />Sixth option
												<option />Seventh option
												<option />Eighth option
											</select>
										</div>
									</div>
									<div class="control-group">
										<label class="control-label">Multiple Select input</label>
										<div class="controls">
											<select multiple="">
												<option />First option
												<option selected="" />Second option
												<option />Third option
												<option />Fourth option
												<option />Fifth option
												<option />Sixth option
												<option />Seventh option
												<option />Eighth option
											</select>
										</div>
									</div>
                                    <div class="control-group">
                                        <label class="control-label">Color picker (hex)</label>
                                        <div class="controls">
                                            <input type="text" data-color="#000000" value="#000000" class="colorpicker input-small" />
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <label class="control-label">Color picker (rgba)</label>
                                        <div class="controls">
                                            <input type="text" data-color="rgba(244,202,56,0.5)" value="rgba(244,202,56,0.5)" data-color-format="rgba" class="colorpicker" />
                                        </div>
                                    </div>                                        
                                    <div class="control-group">
                                        <label class="control-label">Date picker</label>
                                        <div class="controls">
                                            <input type="text" data-date="12-02-2012" data-date-format="dd-mm-yyyy" value="12-02-2012" class="datepicker" />
                                        </div>
                                    </div>
									<div class="control-group">
										<label class="control-label">Radio inputs</label>
										<div class="controls">
											<label><input type="radio" name="radios" /> First One</label>
											<label><input type="radio" name="radios" /> Second One</label>
											<label><input type="radio" name="radios" /> Third One</label>
										</div>
									</div>
									<div class="control-group">
										<label class="control-label">Checkboxes</label>
										<div class="controls">
											<label><input type="checkbox" name="radios" /> First One</label>
											<label><input type="checkbox" name="radios" /> Second One</label>
											<label><input type="checkbox" name="radios" /> Third One</label>
										</div>
									</div>
									<div class="control-group">
										<label class="control-label">File upload input</label>
										<div class="controls">
											<input type="file" />
										</div>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>

<?php include 'component/footer.php';?>